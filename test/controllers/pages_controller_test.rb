require 'test_helper'

describe PagesController do
	fixtures(:users)

	describe "PATCH :accept_consent" do
		before do
			@user = users(:two)
    	ApplicationController.any_instance.stubs(:current_user).returns(@user)
    	patch :accept_consent
		end

		it "updates the user after accepting the consent form" do
			assert_equal(@user.reload.consent, true)
		end
	end

	describe "PATCH :deactivate_user" do
		before do
			@user = users(:two)
    	ApplicationController.any_instance.stubs(:current_user).returns(@user)
    	patch :deactivate_user
		end

		it "sets last_login_at and current_login_at to nil" do
			@user.current_login_at.must_be_nil
			@user.last_login_at.must_be_nil
		end
	end

end