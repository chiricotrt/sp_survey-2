def template(from, to)
  erb = File.read(File.expand_path("../templates/#{from}", __FILE__))
  put ERB.new(erb).result(binding), to
end

def set_default(name, *args, &block)
  set(name, *args, &block) unless exists?(name)
end

namespace :deploy do
  desc "Install everything onto the server"
  task :install do
    run "#{sudo} apt-get -y update"
    run "#{sudo} apt-get -y install python-software-properties wget"
  end

  task :cold do
    transaction do
      update
      setup_db  #replacing migrate in original
      start
    end
  end

  task :setup_db, :roles => :app, :only => {:primary => true} do
    raise RuntimeError.new('db:setup aborted!') unless Capistrano::CLI.ui.ask("About to `rake db:setup`. Are you sure to wipe the entire database (anything other than 'yes' aborts):") == 'yes'
    run "cd #{current_path}; bundle exec rake db:setup RAILS_ENV=#{rails_env}"
  end
end

namespace :rake_exec do
  desc "Runs a given rake task on the server. Example usage: cap rake:invoke COMMAND='db:migrate'"
  task :invoke do
    if ENV['COMMAND'].to_s.strip == ''
      puts "USAGE:   cap rake:invoke COMMAND='db:migrate'"
    else
      run "cd #{current_path} && RAILS_ENV=#{rails_env} bundle exec rake #{ENV['COMMAND']}"
    end
  end
end
