module SectionsHelper

  def pre_section
    @js_variables = {}
  end

  def post_section

  end

  private

  def set_variables
    @variables = {}
    @parameters = {}
  end

  def javascript_exists?(section)
    script = "#{Rails.root}/app/assets/javascripts/sections/#{section}"
    File.exists?("#{script}.js") || File.exists?("#{script}.coffee") ||
    File.exists?("#{script}.js.coffee")
  end

end
