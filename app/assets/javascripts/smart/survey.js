	//jQuery.noConflict();

	function clearAnswers(){
		jQuery("input:radio").each(function() {
			jQuery(this).removeAttr('checked');
		});

		jQuery("input:checkbox").each(function() {
			jQuery(this).removeAttr('checked');
		});
		jQuery('select').each(function() {
			if (jQuery(this).attr('name') != 'member_selection'){
				jQuery(this)[0].selectedIndex = 0;
			}
		});
		//checkGroups("input");
		//checkGroups("select");
	}

	function transformStringIntoId(str){
		str1 = ""+str
		str1 = str1.toLowerCase();
		str1 = str1.split(' ').join('');
		str1 = str1.replace (/\'/g, "");
		return str1
	}

	function loadAnswers(id){
		jQuery.getJSON( '/survey/getPersonData', {userid: id}, function(data, textStatus, jqXHR){
			for(var i=0;i<data.length;i++){
				switch(data[i].type){
				case 'multiple':
  					//alert(data[i].content);
  					to_select = data[i].content.split(',');
  					for(var j=0;j<to_select.length;j++){
  						id = transformStringIntoId(to_select);
  						jQuery("#"+id).attr('checked', 'checked');
  					}
  					break;
				case 'combo_box':
					//alert(data[i].question_id+"_"+data[i].answer_id);
					if (data[i].special_id != ''){
						jQuery("#"+data[i].special_id).val(data[i].content);
						//jQuery("#"+data[i].special_id).trigger('change');
					}
					else{
						jQuery("#"+data[i].question_id).val(data[i].content);
						//jQuery("#"+data[i].question_id).trigger('change');
					}
					break;
				case 'option':
					//alert("OPT: "+data[i].question_id+"_"+data[i].answer_id);
					jQuery("#"+data[i].question_id+"_"+data[i].answer_id).attr('checked', 'checked');
					//jQuery("#"+data[i].question_id+"_"+data[i].answer_id).trigger('change');
					break;

				case 'text_input':
					jQuery("#"+data[i].question_id+"_"+data[i].answer_id).attr('checked', 'checked');
					break;

				}

			}
			checkGroups("input");
			checkGroups("select");

			jQuery('option.trigger').parents('select').each(function(){combo_box_handle_trigger({target:this})});
		});


		jQuery("#question_group_"+val).css('display', '');
		jQuery("#hidden_save").css('display','');
	}

	function clearTextBoxes(opt){
		var selOpt = this;
		jQuery(selOpt).parent().parent().find("input").each(function(index, value) {
			if (value != selOpt && value.name == selOpt.name) {
				jQuery('[name="'+value.name+'_text"]').val('');
			}
		});
	}

	function checkGroups(type){

		active_groups = [];
		types_to_check = ['select', 'input'];

		for (var i=0;i<types_to_check.length;i++){
			func='';
			type = types_to_check[i];
			if (type == 'select'){
				func=':selected';
				type='select option';
			}
			else if (type == 'input'){
				func=':checked';
			}
			jQuery(type).each(function(index) {
				sel_elem = jQuery(this).is(func);
				group_str = jQuery(this).attr('require_group_id');
				if(sel_elem && group_str!="" && group_str){
					active_groups.push(group_str);
				}

				jQuery('[id^="group_"]').css("display", "none");
				for(var i=0; i<active_groups.length;i++){
					jQuery('[id^="group_'+active_groups[i]+'"]').css("display", "");
				}
				//console.log(active_groups);
			});
		}
	}

	function updateTables(number_rows, group_str){
		for(i=0;i<number_rows;i++){
			jQuery("#row_"+i).css("display", "");
		}
		for(i=number_rows;i<7;i++){
			jQuery("#row_"+i).css("display", "none");
		}
	}

	function updateBlocks(number_rows, group_str){
		for(i=0;i<number_rows;i++){
			jQuery("#block_"+i).css("display", "");
			if(i%2 == 0){
				jQuery("#clear_"+(i)).css("display", "");
				if(i>0){
					jQuery("#clear_"+(i-1)).css("display", "");
					jQuery("#stripes_"+(i-1)).css("display", "");
				}
			}
		}
		for(i=number_rows;i<7;i++){
			jQuery("#block_"+i).css("display", "none");
			if(i%2 == 0){
				jQuery("#clear_"+(i)).css("display", "none");
				if(i>0){
					jQuery("#clear_"+(i-1)).css("display", "none");
					jQuery("#stripes_"+(i-1)).css("display", "none");
				}
			}
		}
	}

	function submit_exit_survey(caller) {
		jQuery('#exit_survey_form').attr('action', jQuery(caller).attr('path'));
		jQuery('#exit_survey_form').submit();
	};
