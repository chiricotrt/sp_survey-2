# MAIN CALL #
jQuery ->
  setDependencies()
  setAnswers()

## SETTING DEPENDENCIES ##
setDependencies = ->
  # Dependencies
  # if car-club=yes
  $("#pre_survey_shared_mobility_characteristic_aware_of_car_club_1").click ->
    $(".pre_survey_shared_mobility_characteristic_member_of_car_club").show()

  # if car-club=no
  $("#pre_survey_shared_mobility_characteristic_aware_of_car_club_0").click ->
    $(".pre_survey_shared_mobility_characteristic_member_of_car_club").hide()

  # if member of car-club
  $("#pre_survey_shared_mobility_characteristic_member_of_car_club_1").click ->
    $("#car_sharing_member_questions").show()

  # if not member of car-club
  $("#pre_survey_shared_mobility_characteristic_member_of_car_club_0").click ->
    $("#car_sharing_member_questions").hide()

  # if aware of bike-sharing
  $("#pre_survey_shared_mobility_characteristic_aware_of_bike_sharing_1").click ->
    $("#bike_sharing_member_questions").show()

  # if not aware of bike-sharing
  $("#pre_survey_shared_mobility_characteristic_aware_of_bike_sharing_0").click ->
    $("#bike_sharing_member_questions").hide()

## SETTING ANSWERS ##
setAnswers = ->
  # if aware of car-club
  $(".pre_survey_shared_mobility_characteristic_member_of_car_club").toggle($("#pre_survey_shared_mobility_characteristic_aware_of_car_club_1").prop("checked"))

  # if member of car-club
  $("#car_sharing_member_questions").toggle($("#pre_survey_shared_mobility_characteristic_member_of_car_club_1").prop("checked"))

  # if aware of bike-sharing
  $("#bike_sharing_member_questions").toggle($("#pre_survey_shared_mobility_characteristic_aware_of_bike_sharing_1").prop("checked"))