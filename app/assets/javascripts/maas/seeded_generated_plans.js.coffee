## MAIN CALL
jQuery ->
  # Set up tooltip
  $(document).tooltip()

  # Disable flexible plan radio button (should be enabled only if options chosen)
  $("#picked_plan_plan_d").attr("disabled", true)

  # On change of taxi slider
  $("#flexi_taxi_attribute").change ->
    $("#flexi_taxi_value").html($("#flexi_taxi_attribute").val())
    $("#flexible_plan_taxi_miles").val($("#flexi_taxi_attribute").val())
    updateFlexiblePlanPrice()

  # On change of car club slider
  $("#flexi_car_club_hours").change ->
    $("#flexi_car_club_hours_value").html($("#flexi_car_club_hours").val())
    $("#flexible_plan_car_club_hours").val($("#flexi_car_club_hours").val())
    updateFlexiblePlanPrice()
  $("#flexi_car_club_days").change ->
    $("#flexi_car_club_days_value").html($("#flexi_car_club_days").val())
    $("#flexible_plan_car_club_days").val($("#flexi_car_club_days").val())
    updateFlexiblePlanPrice()

  # On change of public transport plan
  $("input[name='flexi_pt_attribute']:radio").change ->
    $("#flexible_plan_public_transport_plan").val($("input[name='flexi_pt_attribute']:radio:checked").val())
    updateFlexiblePlanPrice()

  # On change of bike sharing plan
  $("input[name='flexi_bike_sharing_attribute']:radio").change ->
    $("#flexible_plan_bike_share_plan").val($("input[name='flexi_bike_sharing_attribute']:radio:checked").val())
    updateFlexiblePlanPrice()

  # On change of likelihood slider
  $("#likelihood").change ->
    $("#picked_plan_likelihood").val($("#likelihood").val())

  # On selection of fixed plan
  # $(".fixed_plan").click ->
  #   resetFlexiblePlanForm()

  # On submit of form
  $("#select_plans").submit ->
    if not $("input[name='picked_plan']:radio:checked").val()
      alert("Kindly select from one of the four plans!")
      return false

    if not $("input[name='buy_chosen_plan']:radio:checked").val()
      alert("Kindly answer the question 'Would you buy your chosen plan if it were available today?'")
      return false

updateFlexiblePlanPrice = () ->
  flexi_car_club = parseInt($("#flexi_car_club_hours").val()) + parseInt($("#flexi_car_club_days").val())
  console.log "flexi_car_club -> #{flexi_car_club}"
  # NOTE: Price should be updated only if at least 2 modes have non-zero value.
  values_array = [
    $("#flexible_plan_public_transport_plan").val(),
    $("#flexible_plan_bike_share_plan").val(),
    $("#flexi_taxi_attribute").val(),
    "#{flexi_car_club}"
  ]

  non_null_array = values_array.filter (value) -> (value != "" and value != "0" and value != "none")
  console.log "non_null -> " + non_null_array.length
  if non_null_array.length < 2
    # Reset price
    $("#flexible_plan_price").html("TBD")
    $("#flexible_plan_subscription_fee").val(0)
    $("#picked_plan_plan_d").prop('checked', false)
    $("#picked_plan_plan_d").attr("disabled", true)
    return

  # *
  # Return if both radio button groups are not selected
  # if not $("input[name='flexi_pt_attribute']:radio:checked").val() or
  # not $("input[name='flexi_bike_sharing_attribute']:radio:checked").val()
  #   return

  # Get seed data and plan attribute values (create hash)
  flexible_plan_values = {
    flexible_plan: {
      seed_data_oyster_zones: $("#oyster_zones").val(),
      seed_data_reduced_fare: $("#preducedfare").val(),
      public_transport_value: $("#flexible_plan_public_transport_plan").val(),
      taxi_value: $("#flexi_taxi_attribute").val(),
      car_club_value: { hours: $("#flexi_car_club_hours").val(), days: $("#flexi_car_club_days").val() },
      bike_sharing_value: $("#flexible_plan_bike_share_plan").val(),
      multiplier: $("#flexible_plan_multiplier").val()
    }
  }

  request = $.get("calculate_flexible_plan_price", flexible_plan_values)
  request.success (data) ->
    # $("#flexible_plan_price").html("&pound;" + data["price"])
    $("#flexible_plan_price").html(data["price"])
    $("#flexible_plan_subscription_fee").val(data["price"])
    $("#picked_plan_plan_d").attr("disabled", false)

resetFlexiblePlanForm = () ->
  # Reset price
  $("#flexible_plan_price").html("TBD")

  # Reset radio buttons
  $("input[name='flexi_pt_attribute']:radio:checked").prop("checked", false)
  $("input[name='flexi_bike_sharing_attribute']:radio:checked").prop("checked", false)

  # Reset sliders and text
  $("#flexi_taxi_attribute").val(0)
  $("#flexi_car_club_hours").val(0)
  $("#flexi_car_club_days").val(0)

  $("#flexi_taxi_value").html(0)
  $("#flexi_car_club_hours_value").html(0)
  $("#flexi_car_club_days_value").html(0)
