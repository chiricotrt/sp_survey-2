class AddResidentialTypeToPreSurveySocioEconomicCharacteristic < ActiveRecord::Migration
  def change
    add_column :pre_survey_socio_economic_characteristics, :residential_type, :string
  end
end
