class RemoveAssociationIdsFromPlansTasks < ActiveRecord::Migration
  def change
    remove_column :plans, :task_id
    remove_column :tasks, :plan_id
  end
end
